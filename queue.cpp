#include <queue>
#include <iostream>
#include <string>
using namespace std;

void QueueProgram()
{
	cout << endl << string(60, '-') << endl << "QUEUE PROGRAM" << endl << endl;

	// DECLARE STACK OF STRINGS

	queue<string> wordQueue;

	// DECLARE COMMAND AND INPUT STRINGS

	string command;
	string input;

	// DISPLAY THE COMMANDS

	cout << "COMMANDS: " << endl;
	cout << "PUSH word				-- Add word to the top of the stack" << endl;
	cout << "POP					-- Remove the item at the top" << endl;
	cout << "STOP					-- End the program and view the stack" << endl;



	// 1. CREATE A PROGRAM LOOP


	bool done = false;
	while (!done)
	{
		cout << "Current Queue Size: " << wordQueue.size();

		if (wordQueue.size() > 0)
		{
			cout << wordQueue.front() << endl;
		}

		cout << "\nEnter PUSH, POP, or STOP: ";
		cin >> command;
		cout << endl;

		if (command == "STOP")
		{
			done = true;
		}
		else if (command == "PUSH")
		{
			cout << "Add a word: ";
			cin >> input;
			wordQueue.push(input);
			cout << endl;
		}
		else if (command == "POP" && wordQueue.size() > 0)
		{
			wordQueue.pop();
		}




	}

	cout << endl << "FINAL QUEUE FORM:" << endl;
	int counter = 1;
	while (wordQueue.size() > 0)
	{
		cout << counter << "\t"
			<< wordQueue.front() << endl;
		wordQueue.pop();
		counter++;
	}

}
