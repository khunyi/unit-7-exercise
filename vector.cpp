#include <vector>
#include <iostream>
#include <string>
using namespace std;

void VectorProgram()
{
	cout << endl << string(60, '-') << endl << "VECTOR PROGRAM" << endl << endl;

	vector<string> wordVector;
	int item_count = 0;


	string word;
	bool done = false;
	while (!done)
	{
		cout << "\nCURRENT VECTOR: " << endl;

		for (int i = 0; i < wordVector.size(); i++)
		{
			cout << "wordVector[" << i << "] = " << wordVector[i] << endl;

		}

		cout << "\nTotal items in vector: " << item_count << endl;

		cout << "Enter a WORD to add to the vector or STOP to stop: ";
		cin >> word;
		cout << endl;

		if (word == "STOP")
		{
			done = true;
		}
		else
		{
			wordVector.push_back(word);
			item_count++;


		}

	}

}
