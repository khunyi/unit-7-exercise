#include <map>
#include <iostream>
#include <string>
using namespace std;

void MapProgram()
{
	cout << endl << string(60, '-') << endl << "MAP PROGRAM" << endl << endl;

	// 1. DECLARE A MAP with int keys and string values.

	map<int, string> studentMap;

	// INITIALIZE THE MAP WITH THE DATA FROM THE DOCUMENTATION.

	studentMap[1068] = "Kaz Brekker";
	studentMap[5732] = "Inej Ghafa";
	studentMap[1538] = "Jesepr Fahey";
	studentMap[8857] = "Nina Zenik";

	// 2. DISPLAY ALL KEY-VALUE PAIRS IN THE MAP

	cout << "ALL ITEMS IN MAP: " << endl;

	for (auto& item : studentMap)
	{
		cout << "KEY: " << item.first << " VALUE: " << item.second << endl;
	}

	// 3. ASK THE USER TO ENTER AN ID

	int key;
	cout << "\nEnter a student ID: ";
	

	// 4. TRY TO ACCESS THE STUDENT AT THAT KEY

	if (cin >> key)
	{
		try
			{
				cout << "That student is " << studentMap.at(key);
			}

	// 5. CATCH ANY EXCEPTIONS AND DISPLAY AN ERROR MESSAGE

		catch (out_of_range&)
			{
				cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl << "!!!!!!!!!!ERROR!!!!!!!!!!!"
					<< endl << "!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
			}
	}
	
}
