#include <stack>
#include <iostream>
#include <string>
using namespace std;

void StackProgram()
{
	cout << endl << string(60, '-') << endl << "STACK PROGRAM" << endl << endl;

	// DECLARE STACK OF STRINGS

	stack<string> wordStack;

	// DECLARE COMMAND AND INPUT STRINGS

	string command;
	string input;

	// DISPLAY THE COMMANDS

	cout << "COMMANDS: " << endl;
	cout << "PUSH word				-- Add word to the top of the stack" << endl;
	cout << "POP					-- Remove the item at the top" << endl;
	cout << "STOP					-- End the program and view the stack" << endl;



	// 1. CREATE A PROGRAM LOOP


	bool done = false;
	while (!done)
	{
		cout << "Current Stack Size: " << wordStack.size();

		if (wordStack.size() > 0)
		{
			cout << wordStack.top() << endl;
		}

		cout << "\nEnter PUSH, POP, or STOP: ";
		cin >> command;
		cout << endl;

		if (command == "STOP")
		{
			done = true;
		}
		else if (command == "PUSH")
		{
			cout << "Add a word: ";
			cin >> input;
			wordStack.push(input);
			cout << endl;
		}
		else if (command == "POP" && wordStack.size() > 0)
		{
			wordStack.pop();
		}




	}

	cout << endl << "FINAL STACK FORM:" << endl;
	int counter = 1;
	while (wordStack.size() > 0)
	{
		cout << counter << "\t"
			<< wordStack.top() << endl;
		wordStack.pop();
		counter++;
	}

}
