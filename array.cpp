#include <array>
#include <iostream>
#include <string>
using namespace std;

void TraditionalArrayProgram()
{
	cout << endl << string(60, '-') << endl << "TRADITIONAL ARRAY PROGRAM" << endl << endl;

	// 1. VARIABLES: Start off by declaring the following items:
    // ARR_SIZE, a const int with a value of 5.
    // itemCount, an integer with a value of 0.
    // wordArray, an array of strings of size ARR_SIZE.

	const int ARR_SIZE = 5;
	int itemCount = 0;
	string wordArray[ARR_SIZE];




    // 2. CREATE A BASIC PROGRAM LOOP


		// 3. DISPLAY ALL ELEMENTS OF THE ARRAY


		// 4. ASK USER TO ENTER A NEW ITEM TO PUT IN ARRAY, OR STOP TO END.


        // 5. CHECK IF THE ARRAY IS FULL. IF NOT FULL, ADD NEW ITEM TO ARRAY.


	string word;
	bool done = false;
	while (!done)
	{
		cout << "\nCURRENT ARRAY: " << endl;

		for (int i = 0; i < ARR_SIZE; i++)
		{
			cout << "wordArray[" << i << "] = " << wordArray[i] << endl;
			
		}

		cout << "\nTotal items in array: " << itemCount << "/5" << endl;

		cout << "Enter a WORD to add to array or STOP to stop: ";
		cin >> word;
		cout << endl;

		if (word == "STOP") 
		{
			done = true;
		}
		else
		{
			if (itemCount < ARR_SIZE) 
			{
				wordArray[itemCount++] = word;
			}
			else
			{
				cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl << "!! ERROR! Array is full !!" 
					<< endl << "!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
			}
			
		}

	}

}

void ArrayObjectProgram()
{
	cout << endl << string(60, '-') << endl << "ARRAY OBJECT PROGRAM" << endl << endl;

	int item_count = 0;
	array<string, 5> word_array;



	string word;
	bool done = false;
	while (!done)
	{
		cout << "\nCURRENT ARRAY: " << endl;

		for (int i = 0; i < word_array.size(); i++)
		{
			cout << "wordArray[" << i << "] = " << word_array[i] << endl;

		}

		cout << "\nTotal items in array: " << item_count << "/5" << endl;

		cout << "Enter a WORD to add to the array or STOP to stop: ";
		cin >> word;
		cout << endl;

		if (word == "STOP")
		{
			done = true;
		}
		else
		{
			if (item_count < word_array.size())
			{
				word_array[item_count++] = word;
			}
			else
			{
				cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl << "!! ERROR! Array is full !!" 
					<< endl << "!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
			}

		}

	}


}
