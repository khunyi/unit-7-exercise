#include <list>
#include <iostream>
#include <string>
using namespace std;

void ListProgram()
{
	cout << endl << string(60, '-') << endl << "LIST PROGRAM" << endl << endl;

	list<string> wordList;

	int item_count = 0;


	string word;
	string insert;
	bool done = false;
	while (!done)
	{
		cout << "\nCURRENT LIST: " << endl;

		for (auto& item : wordList)
		{
			cout << item << endl;
		}

		cout << "\nTotal items in list: " << item_count << endl;

		cout << "Enter a WORD to add to the list or STOP to stop: ";
		cin >> word;

		cout << "Enter in the FRONT or BACK: ";
		cin >> insert;
		cout << endl;

		if (word == "STOP")
		{
			done = true;
		}
		else
		{
			if (insert == "FRONT")
			{
				wordList.push_front(word);
				item_count++;
			}
			else
			{
				wordList.push_back(word);
				item_count++;
			}
			


		}

	}



}
